﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscillator : MonoBehaviour {

    [SerializeField] private Vector3 _movementVector = new Vector3(10f, 10f, 10f);
    [SerializeField] private float _period = 2f;

    [Range(0,1)] [SerializeField] private float _movementFactor;

    private Vector3 _startingPos;

    // Use this for initialization
    void Start () {
        _startingPos = transform.position;

    }
    
    // Update is called once per frame
    void Update () {
        //todo protect against period is zero
        if (_period <= Mathf.Epsilon) return;
        var cycles = Time.time / _period; // grows continually from zero

        const float tau = 2 * Mathf.PI;
        var sinWave = Mathf.Sin(cycles * tau);

        _movementFactor = sinWave / 2f + 0.5f;

        var offset = _movementFactor * _movementVector;
        transform.position = _startingPos + offset;        
    }
}
