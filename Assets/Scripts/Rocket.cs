﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour {

    [SerializeField] private float _rcsThrust;
    [SerializeField] private float _mainThrust;
    [SerializeField] private AudioClip _mainEngine;
    [SerializeField] private AudioClip _deathSound;
    [SerializeField] private AudioClip _successSound;

    [SerializeField] private ParticleSystem _mainEngineVFX;
    [SerializeField] private ParticleSystem _successVFX;
    [SerializeField] private ParticleSystem _deathVFX;

    //[SerializeField] private int _nextLvlIdx = 1;


    private Rigidbody _rigidbody;
    private AudioSource _audioSource;
    private bool _collisionsAreEnabled;

    enum State { Alive, Dying, Transcending};
    private State _state = State.Alive;

    // Use this for initialization
    void Start () {
        _rigidbody = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();
    }
    
    // Update is called once per frame
    void Update () {
        if (_state == State.Alive)
        {
            RespondToThrustInput();
            RepondToRotateInput();
        }

        if (Debug.isDebugBuild)
        {
            RespondToDebugKeys();
        }
    }

    private void RespondToDebugKeys()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextScene();
        }else if (Input.GetKeyDown(KeyCode.C))
        {
            _collisionsAreEnabled = !_collisionsAreEnabled;
        }
    }

    private void RepondToRotateInput()
    {
        _rigidbody.angularVelocity = Vector3.zero;

        var haxis = Input.GetAxis("Horizontal");
        var rotationThisFrame = _rcsThrust * Time.deltaTime;

        if (haxis < 0)
        {
            transform.Rotate(Vector3.forward * rotationThisFrame);
        }
        else if (haxis > 0)
        {
            transform.Rotate(-Vector3.forward * rotationThisFrame);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (_state != State.Alive || _collisionsAreEnabled) {return;} //ignore collisions when dead

        switch (collision.gameObject.tag)
        {
            case "Friendly":
                print("Ok"); //todo remove
                break;
            case "Finish":
                StartSuccessSeqence();
                break;
            default:
                StartDeathSequence();
                break;
        }
    }

    private  void RestartThisLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void LoadNextScene() //todo allow more than two levels
    {
        var nextSceneIdx = (SceneManager.GetActiveScene().buildIndex + 1) % SceneManager.sceneCountInBuildSettings;
        SceneManager.LoadScene(nextSceneIdx);
    }

    private void RespondToThrustInput()
    {
        if (Input.GetButton("Jump"))
        {
            ApplyThrust();
        }
        else
        {
            StopApplyingThrust();
        }
    }

    private void StopApplyingThrust()
    {
        _audioSource.Stop();
        _mainEngineVFX.Stop();
    }

    private void ApplyThrust()
    {
        _rigidbody.AddRelativeForce(Vector3.up * _mainThrust * Time.deltaTime);
        if (!_audioSource.isPlaying)
        {
            _audioSource.PlayOneShot(_mainEngine);
        }
        _mainEngineVFX.Play();
    }

   private void StartDeathSequence()
    {
        print("Hit something deadly");
        _state = State.Dying;

        _audioSource.Stop();
        _audioSource.PlayOneShot(_deathSound);
        _deathVFX.Play();

        Invoke("RestartThisLevel", 1f); //parametarise time
    }
    private void StartSuccessSeqence()
    {
        print("Finish");
        _state = State.Transcending;

        _audioSource.Stop();
        _audioSource.PlayOneShot(_successSound);
        _successVFX.Play();

        Invoke("LoadNextScene", 1f); //parametarise time
    }
}
